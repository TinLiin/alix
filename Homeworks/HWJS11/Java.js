"use strict";
let lisst = document.getElementsByTagName("button");
document.addEventListener("keypress", el);
function el(e) {
  const press = e.key.toUpperCase();
  for (let index = 0; index < lisst.length; index++) {
    let element = lisst[index];
    element.className = "btn";
    if (press === element.innerHTML.toUpperCase()) {
      element.className = "btnActive";
    }
  }
  }
