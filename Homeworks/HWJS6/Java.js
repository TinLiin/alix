"use strict";

function filterBy(arr, type) {
  return arr.filter((e) => typeof(e) !== type);
};
console.log(filterBy([45, 44, "ee", null, undefined, NaN, "Uasya", 55, 49], "string")); 
